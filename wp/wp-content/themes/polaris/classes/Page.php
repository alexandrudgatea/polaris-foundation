<?php
namespace classes;
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18.11.2016
 * Time: 12:37
 */
class Page
{
    public $id;
    public $title;
    public $hero;
    public $content;
    public $meta;

    public function __construct($id) {
        $this->id = $id;
        $this->title = get_the_title($this->id);
        $this->hero = wp_get_attachment_url( get_post_thumbnail_id($this->id));
        $this->meta = get_post_custom($this->id);
        $this->content = get_post_field('post_content', $this->id);
    }
}