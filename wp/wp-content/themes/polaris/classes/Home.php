<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 21.11.2016
 * Time: 11:29
 */

namespace classes;
use WP_Query;

class Home
{
    public $id;
    public $title;
    public $hero;
    public $content;
    public $meta;
    public $parteneri;

    public function __construct($id) {
        $this->id = $id;
        $this->title = get_the_title($this->id);
        $this->hero = wp_get_attachment_url( get_post_thumbnail_id($this->id));
        $this->meta = get_post_custom($this->id);
        $this->content = get_post_field('post_content', $this->id);
        $args = array(
            'post_type' => 'partener',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'ignore_sticky_posts' => 1,
            'orderby' => 'title',
            'order' => 'ASC',
            'fields' => 'ids'
        );
        $query = new WP_Query($args);
        foreach ($query->posts as $post) {
            $this->parteneri[] = array(
                'logo' => wp_get_attachment_url( get_post_thumbnail_id($post)),
                'link' => get_post_meta($post, 'link_partener', true)
            );
        }
    }
}