<?php
namespace classes;

/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18.11.2016
 * Time: 12:40
 */
class Partner
{

    public $id;
    public $title;
    public $logo;
    public $link;

    public function __construct($id) {
        $this->id = $id;
        $this->title = get_the_title($this->id);
        $this->logo = wp_get_attachment_url( get_post_thumbnail_id($this->id));
        $this->link = get_post_meta($this->id, 'link_partener', true);
    }

}