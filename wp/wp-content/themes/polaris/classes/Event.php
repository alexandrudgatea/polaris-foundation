<?php
namespace classes;

/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18.11.2016
 * Time: 12:35
 */
class Event
{
    public $id;
    public $title;
    public $hero;
    public $location;
    public $content;
    public $date;
    public $gallery;
    public $testimonials;
    public $permalink;
    public $image;
    public $intro;

    public function __construct($id) {
        $this->id = $id;
        $this->title = get_the_title($this->id);
        $this->hero = wp_get_attachment_url( get_post_thumbnail_id($this->id));
        $this->content = get_post_field('post_content', $this->id);
        $this->permalink = get_the_permalink($this->id);
        $this->location = get_post_meta($this->id, 'locatie_eveniment', true);
        $this->date = get_post_meta($this->id, 'data_eveniment', true);
        $this->image = wp_get_attachment_url(get_post_meta($this->id, 'imagine', true));
        $this->intro = get_post_meta($this->id, 'intro', true);
        $this->gallery = get_post_meta($this->id, 'galerie_eveniment', true);
        $testimonial_nr =  $nr_nevoi_pachet_pj = get_post_meta($this->id, 'testimoniale', true);
        for ($i = 0; $i < $testimonial_nr; $i++) {
            $this->testimonials[] = array(
                'nume' => get_post_meta($this->id, 'testimoniale_' . $i . '_nume', true),
                'img' => get_post_meta($this->id, 'testimoniale_' . $i . '_imagine', true),
                'text' => get_post_meta($this->id, 'testimoniale_' . $i . '_text_testimonial', true)
            );
        }
    }
}