<?php
/**
 * Template name: Voluntariat
 */
get_header();
$voluntariat = new classes\Page(get_the_ID());
?>
<div id="volunteer" class="page">
    <section id="hero" style="background-image: url('<?php print $voluntariat->hero; ?>')" data-uk-parallax="{bg: '150'}">
    </section>

    <div class="uk-container uk-container-center">
        <div class="uk-grid">
            <div class="uk-width-1-2" data-uk-scrollspy="{cls:'uk-animation-slide-left'}">
                <div class="vol-wrap">
                    <h1><?php print $voluntariat->meta['titlu_pagina'][0]; ?></h1>
                    <span class="block intro">
                        <?php print $voluntariat->meta['sumar_pagina'][0]; ?>
				    </span>
                    <div>
                        <?php print $voluntariat->content; ?>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-2" data-uk-scrollspy="{cls:'uk-animation-slide-right'}">
                <div class="vol-images">
                    <div class="uk-grid">
                        <div class="uk-width-2-3">
                            <div class="img img1" style="background-image: url('/wp-content/themes/polaris/images/v_img1.jpg')"></div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="uk-grid">
                                <div class="uk-width-1-1">
                                    <div class="img img2" style="background-image: url('/wp-content/themes/polaris/images/v_img2.jpg')"></div>
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-1-1">
                                    <div class="img img2 dark-green-bg"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div class="img img3" style="background-image: url('/wp-content/themes/polaris/images/v_img3.jpg')"></div>
                        </div>
                    </div>
                    <div class="uk-grid">
                        <div class="uk-width-1-3">
                            <div class="img img3 green-bg"></div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="img img3" style="background-image: url('/wp-content/themes/polaris/images/v_img4.jpg')"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php get_footer(); ?>