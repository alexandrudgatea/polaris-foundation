<?php
/**
 * Template name: Educatie
 */
get_header();
$educatie = new classes\Page(get_the_ID());
?>
    <div id="blog" class="page">
        <section id="hero" style="background-image: url('<?php print $educatie->hero; ?>')"
                 data-uk-parallax="{bg: '150'}">

        </section>
        <section class="event-details" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <h1 class="event-name"><?php print $educatie->meta['titlu_pagina'][0]; ?></h1>
                    </div>
                    <div class="uk-width-1-2">
                        <div class="block intro">
                            <?php print $educatie->content; ?>
                        </div>
                    </div>
                    <div class="uk-width-1-2">
                        <img src="/wp-content/themes/polaris/images/blog-image.jpg" class="uk-float-right event-image">
                    </div>
                    <div class="uk-width-1-1">
                        <div class="details">
							<span class="block">
<!--                                 --><?php //print $educatie->content; ?>
							</span>
                            <!--                        <p>Autor: <span>Dr. Mihaela Popescu</span></p>-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php get_footer(); ?>