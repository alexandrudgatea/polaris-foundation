<?php
/**
 * Template name: Home
 */
get_header();
$home = new classes\Home(get_the_ID()); ?>

    <div id="homepage" class="page">
        <section id="hero" style="background-image: url('<?php print $home->hero; ?>')" data-uk-parallax="{bg: '100'}">
            <div class="uk-container uk-container-center uk-clearfix">
                <div class="uk-grid">
                    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2">
                        <div class="hero-content uk-vertical-align-bottom" >
                            <h1><?php print $home->meta['titlu_hero'][0]; ?></h1>
                            <span class="uk-vertical-align-bottom"><?php print $home->content; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-view">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2" data-uk-scrollspy="{cls:'uk-animation-slide-left'}">
                        <div class="card">
                            <div class="card-image" style="background-image: url('<?php print wp_get_attachment_url($home->meta['imagine-evenimente'][0]); ?>')"></div>
                            <div class="card-content uk-text-center">
                                <h1><?php print $home->meta['titlu-evenimente'][0]; ?></h1>
                                <ul class="event-details inline-block uk-text-center">
                                    <li><i class="uk-icon-calendar-o green"></i> <?php print $home->meta['data-evenimente'][0]; ?></li>
                                    <li><i class="uk-icon-map-marker green"></i> <?php print $home->meta['locatie-evenimente'][0]; ?></li>
                                </ul>
                                <a href="<?php print get_the_permalink($home->meta['link_eveniment'][0]); ?>" class="uk-button dark-green-bg">Detalii</a>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2" data-uk-scrollspy="{cls:'uk-animation-slide-right'}">
                        <div class="card">
                            <div class="card-content uk-text-center">
                                <h1><?php print $home->meta['titlu-donatii'][0]; ?></h1>
                                <span><?php print $home->meta['descriere-donatii'][0]; ?></span>
                                <a href="<?php print get_the_permalink($home->meta['link_donatie'][0]); ?>" class="uk-button red-bg">Doneaza</a>
                            </div>
                            <div class="card-image" style="background: url('<?php print wp_get_attachment_url($home->meta['imagine-donatii'][0]); ?>')"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="volunteer" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}" style="background: url('<?php print get_template_directory_uri(); ?>/images/green_bg.jpg')">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2 uk-text-center uk-vertical-align">
                        <div class="content uk-vertical-align-middle">
                            <h1><?php print $home->meta['titlu-voluntariat'][0]; ?></h1>
                            <span class="block"><?php print $home->meta['descriere-voluntariat'][0]; ?> </span>
                            <a href="<?php print get_the_permalink($home->meta['link_voluntariat'][0]); ?>" class="uk-button dark-green-bg">Vreau sa fiu voluntar</a>
                        </div>

                    </div>
                    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2">
                        <ul class="inline-block images-grid uk-text-right">
                            <li>
                                <div class="element uk-float-left" style="background: url('<?php print get_template_directory_uri(); ?>/images/home_volunteer_2.jpg')"></div>
                            </li>
                            <li>
                                <div class="element uk-float-right" style="background: url('<?php print get_template_directory_uri(); ?>/images/home_volunteer_1.jpg')"></div>
                            </li>
                            <li>
                                <div class="element uk-float-left" style="background: url('<?php print get_template_directory_uri(); ?>/images/home_volunteer_4.jpg')"></div>
                            </li>
                            <li>
                                <div class="element uk-float-right" style="background: url('<?php print get_template_directory_uri(); ?>/images/home_volunteer_3.jpg')"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="blog uk-vertical-align" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
            <div class="uk-container uk-container-center ">
                <div class="uk-grid uk-vertical-align-middle">
                    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2">
                        <img src="<?php print wp_get_attachment_url($home->meta['imagine-educatie'][0]); ?>">
                    </div>
                    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2 uk-text-center ">
                        <div class="content ">
                            <h1><?php print $home->meta['titlu-educatie'][0]; ?></h1>
                            <span class="block"><?php print $home->meta['descriere-educatie'][0]; ?></span>
                            <a href="<?php print get_the_permalink($home->meta['link_educatie'][0]); ?>" class="uk-button dark-green-bg">mai mult</a>
                        </div>
                    </div>
                </div>
                <hr class="divider">
            </div>
        </section>
        <section class="partners" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <ul class="inline-block uk-text-center">
                            <?php foreach ($home->parteneri as $part) { ?>
                                <li><a href="<?php print $part['link']; ?>"><img src="<?php print $part['logo']; ?>"></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </div>
<?php get_footer() ?>