<?php
/**
 * Template Name: Donations
 */
get_header();
$donatii = new classes\Donations(get_the_ID());
?>

<div id="donation" class="page">
    <section id="hero" style="background-image: url('<?php print $donatii->hero; ?>')" data-uk-parallax="{bg: '150'}">

    </section>
    <section class="donation-card" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
        <div class="uk-container uk-container-center">
            <div class="uk-grid">
                <div class="uk-width-2-3 uk-container-center">
                    <div class="card">
                        <?php print do_shortcode($donatii->content); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>
