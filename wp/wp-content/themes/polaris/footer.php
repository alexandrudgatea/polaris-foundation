<section id="footer">
    <div class="uk-container uk-container-center">
        <div class="uk-grid">
            <div class="uk-width-small-1-1 uk-width-medium-1-2">
                <div class="error_messages"></div>
                <form class="uk-form" id="newsletterForm" action="" method="post">
                    <fieldset>
                        <legend>Abonaza-te la newsletter</legend>
                        <div class="uk-form-row newsletter-text uk-width-4-6">
                            Esti interesat de activitatea noastra? Aboneaza-te la newsletter si te vom tine la curent cu toate noutatile!
                        </div>
                        <div class="uk-form-row">
                            <input type="text" placeholder="Adresa de email" id="nws-email" name="nws-email">
                            <button type="submit" class="uk-button">Ma abonez</button>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="uk-width-small-1-1 uk-width-medium-1-2">
                <div class="uk-float-right contact-column">
                    <h1>Contacteaza-ne</h1>
                    <ul class="uk-text-right">
                        <li><a href="mailto:contact@fundatiapolaris.ro">contact@fundatiapolaris.ro <i class="uk-icon-envelope"></i></a></li>
                        <li><a href="tel:0745495979">0745495979 <i class="uk-icon-phone"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="uk-grid">
        <div class="uk-width-1-1">
            <ul class="footer-bottom">
                <li>© Copyrighted 2016-2017 </li>
                <li>Fundatia Polaris</li>
                <li>Toate drepturile rezervate.</li>
            </ul>
        </div>
    </div>
</section>
<?php wp_footer(); ?>
<script>
    jQuery(document).ready(function ($) {
        $('#newsletterForm').validate({
            rules: {
                'nws-email': {
                    required: true,
                    email: true
                }
            },
            messages: {
                newsletter_email: {
                    required: "Vă rugăm introduceți adresa de email",
                    email: "Email-ul introdus nu este valid"
                }
            },
            errorPlacement: function (error, element) {
                $('.error_messages').html('').addClass('red');
                error.appendTo($('.error_messages'));
            },
            submitHandler: function () {
                $.ajax({
                    type: 'post',
                    url: '/wp-admin/admin-ajax.php',
                    data: {
                        action: 'nl_signup_callback',
                        newsletter_email: $('#nws-email').val()
                    },
                    success: function (data) {
                        var result = "";
                        switch (data) {
                            case 'email-failed':
                                result = "<label class='error'>A apărut o eroare la înregistrare. Reîncercați cu un email valid.</label>";
                                break;
                            case 'already-exists':
                                result = "<label class='error'>E-mailul introdus a fost deja înregistrat pentru newsletter.</label>";
                                break;
                            default:
                                result = "<label class='success'>Ați fost înscris cu succes la newsletter!</label>";
                        }
                        $('.error_messages').html("");
                        $('.error_messages').html(result);
                        setTimeout(function () {
                            $('.error_messages').html("");
                        }, 3000);
                    }
                });
            }
        });
    });
</script>
</body>

</html>

