<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18.11.2016
 * Time: 12:33
 */
get_header();
$event = new classes\Event(get_the_ID()); ?>

    <div id="event" class="page">
        <section id="hero" style="background-image: url('<?php print $event->hero; ?>')" data-uk-parallax="{bg: '100'}">

        </section>
        <section class="event-details">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <h1 class="event-name"><?php print $event->title; ?></h1>
                        <div class="details">
                            <img src="<?php print $event->image; ?>" class="uk-float-right event-image">
                            <ul class="dark-green-bg inline-block uk-width-small-1-1 uk-width-large-1-2 detail-bar">
                                <li><i class="uk-icon-calendar-o green"></i> <?php print $event->date; ?></li>
                                <li><i class="uk-icon-map-marker green"></i> <?php print $event->location; ?></li>
                            </ul>
							<span class="block intro">
								<?php print $event->intro; ?>
                            </span>
							<span class="block">

								<?php print $event->content; ?>
                            </span>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="mediaGallery" style="background: url('<?php print get_template_directory_uri(); ?>/images/green_bg.jpg')" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <h1>Galerie media</h1>
                        <ul id="box-container" class="media-gallery inline-block">
                            <?php foreach ($event->gallery as $img) { ?>
                                <li class="box">
                                    <a href="<?php print wp_get_attachment_url($img); ?>" class="swipebox" title="Title of image">
                                        <img src="<?php print wp_get_attachment_url($img); ?>" alt="image">
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section id="testimonials" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
            <div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-1-1 uk-text-center">
                        <h1>Testimoniale</h1>
                        <div class="uk-margin" data-uk-slideset="{small: 1, medium: 2, large: 3}">
                            <div class="uk-slidenav-position uk-margin">
                                <ul class="uk-slideset uk-grid uk-flex-center uk-grid-width-1-3">
                                    <?php foreach ($event->testimonials as $testimonial) { ?>
                                    <li>
                                        <div class="testimonial">
                                            <div class="testimonial-image">
                                                <img src="<?php print wp_get_attachment_url($testimonial['img'])?>" class="uk-border-circle">
                                            </div>
                                            <span class="block testimonial-text"><?php print $testimonial['text'];?></span>
                                            <span class="block testimonial-author"> <?php print $testimonial['nume'];?></span>
                                        </div>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <ul class="uk-slideset-nav uk-dotnav uk-flex-center"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            // initialize image lightbox
            $('.swipebox').swipebox();

            // initialize video lightbox
            $('.swipebox-video').swipebox();

            $('.swipebox-video').click(function(e) {
                e.preventDefault();
            });

            // check if the image is has landscape or portrait orientation and stile acordingly
            $(window).load(function() {
                $('#mediaGallery').find('img').each(function() {
                    var imgClass = (this.width / this.height > 1) ? 'wide' : 'tall';
                    $(this).addClass(imgClass);
                });
            });
        });

    </script>

<?php get_footer(); ?>