<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18.11.2016
 * Time: 12:30
 */

include get_template_directory() . '/classes/Event.php';
include get_template_directory() . '/classes/Page.php';
include get_template_directory() . '/classes/Partner.php';
include get_template_directory() . '/classes/Home.php';
include get_template_directory() . '/classes/Donations.php';

function register_my_menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'mobile-menu' => __( 'Mobile Menu' )
        )
    );
}
add_action( 'init', 'register_my_menus' );

function include_scripts() {
    wp_enqueue_style('uikitcss', get_template_directory_uri() . '/css/uikit.min.css');
    wp_enqueue_style('styles', get_template_directory_uri() . '/css/styles.css');
    wp_enqueue_style('query', get_template_directory_uri() . '/css/query.css');
    wp_enqueue_style('swipeboxcss', get_template_directory_uri() . '/css/swipebox.min.css');
    wp_enqueue_style('robotofont', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,700" rel="stylesheet');

    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-2.1.4.min.js', array(), false, true);
    wp_enqueue_script('uikit', get_template_directory_uri() . '/js/uikit.min.js', array(), false, true);
    wp_enqueue_script('parralax', get_template_directory_uri() . '/js/parallax.min.js', array(), false, true);
    wp_enqueue_script('slideset', get_template_directory_uri() . '/js/slideset.min.js', array(), false, true);
    wp_enqueue_script('validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array(), false, true);
    wp_enqueue_script('swipebox', get_template_directory_uri() . '/js/jquery.swipebox.min.js', array(), false, true);
}
add_action( 'wp_enqueue_scripts', 'include_scripts' );

//Newsletter signup
add_action( 'wp_ajax_nl_signup_callback', 'nl_signup_callback' );
add_action( 'wp_ajax_nopriv_nl_signup_callback', 'nl_signup_callback' );
function nl_signup_callback() {
    // Verify email validity
    $email = $_POST["newsletter_email"];
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        wp_die("email-failed");
    }

    // check if not exists
    global $wpdb;
    $post_id = $wpdb->get_var("SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'subscription_email' and meta_value = '" . $email . "'");
    if ($post_id !== null) {
        wp_die('already-exists');
    }

    // Save email in db
    $post_id = wp_insert_post(
        array(
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_status' => 'publish',
            'post_type' => 'newsletter',
            'post_title' => $email
        )
    );
    update_post_meta($post_id, 'subscription_email', $email);

    wp_die('succes'); // this is required to terminate immediately and return a proper response
}

function declaratii( $atts, $nr ) {
    $a = shortcode_atts( array(
        'nr' => $nr
    ), $atts );

    $link = of_get_option('declaratia-' . $a['nr']);
    return $link;
}
add_shortcode( 'declaratie', 'declaratii' );


function special_nav_class($classes, $item){
    $menu_locations = get_nav_menu_locations();
    if ( has_term($menu_locations['header-menu'], 'nav_menu', $item) ||  has_term($menu_locations['mobile-menu'], 'nav_menu', $item) ) {
        if (in_array('current-menu-item', $classes)) {
            $classes[] = 'uk-active';
        }
    }
    return $classes;
}

add_filter('nav_menu_css_class' , 'special_nav_class', 10, 2);


