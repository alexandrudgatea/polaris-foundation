<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18.11.2016
 * Time: 12:40
 */

get_header();
$page = new classes\Page(get_the_ID());

if ( have_posts() ) {
    while ( have_posts() ) {
        the_post();
        //
       the_meta();
        //
    } // end while
} // end if
get_footer();