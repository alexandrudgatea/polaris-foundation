<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fundatia Polaris</title>
    <link rel="shortcut icon" href="">
    <!--[if IE]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body>
<nav class="uk-navbar" id="menu">
    <div class="uk-container uk-container-center">
        <div class="uk-grid">
            <div class="uk-width-1-1">

                <!-- logo visible on large devices-->
                <a href="#!" class="uk-navbar-brand uk-hidden-small">
                    <img src="<?php print get_template_directory_uri(); ?>/images/fundatie_logo.jpg">
                </a>

                <!-- navbar visible on large devices-->
                <?php $defaults = array(
                    'theme_location'  => 'Header Menu',
                    'menu'            => 'Main',
                    'container'       => '',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => 'menu uk-navbar-nav uk-navbar-flip uk-hidden-small desktop-menu',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 0
                );
                ?>

                <?php wp_nav_menu( $defaults ); ?>

                <!-- the toggle of off-canvas side menu-->
                <a href="#mobileSidemenu" data-uk-offcanvas>
                    <span class="uk-navbar-toggle uk-visible-small"></span>
                </a>

                <!-- logo visible on small and medium devices-->
                <a href="#!" class="uk-navbar-brand uk-navbar-center uk-visible-small">
                    <img src="<?php print get_template_directory_uri(); ?>/images/fundatie_logo.jpg">
                </a>
            </div>
        </div>
    </div>
</nav>

<!-- This is the off-canvas sidebar -->
<div id="mobileSidemenu" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">

        <?php $mobile_defaults = array(
            'theme_location'  => 'Mobile Menu',
            'menu'            => 'Mobile',
            'container'       => '',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => 'menu uk-nav',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0
        );
        ?>

        <?php wp_nav_menu( $mobile_defaults ); ?>
        
    </div>
</div>
