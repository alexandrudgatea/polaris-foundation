<?php

/**
 * Call this script from the command line with the first param being the search
 * term and the second one being what it will be repaced with.
 * !!! Don't forget to change DOMAIN_CURRENT_SITE in wp-config.php
 *
 * Example:
 * php script.php "conta.dev" "conta2.dev"
 */

define('DB_FIND_REPLACE', true);

if (php_sapi_name() !== 'cli') {
    exit('This script can only be run from cli.');
}

require realpath(dirname(__FILE__)) . '/../wp-config.php';

$h = DB_HOST;
$n = DB_NAME;
$u = DB_USER;
$p = DB_PASSWORD;
if ($p === '') {
    $p = '""';
}

// Search for.
$s = $argv[1];

// Replace with.
$r = $argv[2];

passthru("php srdb.cli.php -h {$h} -n {$n} -u {$u} -p {$p} -s {$s} -r {$r}");
