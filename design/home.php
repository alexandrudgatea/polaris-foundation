<?php include("templates/header.php") ?>

	<div id="homepage" class="page">
		<section id="hero" style="background-image: url('images/home_hero.jpg')" data-uk-parallax="{bg: '-100'}">
			<div class="uk-container uk-container-center uk-clearfix">
				<div class="uk-grid">
					<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2">
						<div class="hero-content uk-vertical-align-bottom">
							<h1>Fundatia Polaris</h1>
							<span class="uk-vertical-align-bottom">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in. Lorem ipsum dolor sit exercitation ullamco laboris nisi ut aliquip.</span>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="main-view">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">
					<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2" data-uk-scrollspy="{cls:'uk-animation-slide-left'}">
						<div class="card">
							<div class="card-image" style="background: url('images/mv_event.jpg')"></div>
							<div class="card-content uk-text-center">
								<h1>heART paintings event name will be here</h1>
								<ul class="event-details inline-block uk-text-center">
									<li><i class="uk-icon-calendar-o green"></i> 14.12.2016, ora 18:00</li>
									<li><i class="uk-icon-map-marker green"></i> Muzeul de arta Cluj</li>
								</ul>
								<a href="#!" class="uk-button dark-green-bg">Detalii</a>
							</div>
						</div>
					</div>
					<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2" data-uk-scrollspy="{cls:'uk-animation-slide-right'}">
						<div class="card">
							<div class="card-content uk-text-center">
								<h1>Contribuie si tu la Fundatia Polaris</h1>
								<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span>
								<a href="#!" class="uk-button red-bg">Doneaza</a>
							</div>
							<div class="card-image" style="background: url('images/mv_donatii.jpg')"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="volunteer" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}" style="background: url('images/green_bg.jpg')">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">
					<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2 uk-text-center uk-vertical-align">
						<div class="content uk-vertical-align-middle">
							<h1>Implica-te si tu</h1>
							<span class="block">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </span>
							<a href="#!" class="uk-button dark-green-bg">Vreau sa fiu voluntar</a>
						</div>

					</div>
					<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2">
						<ul class="inline-block images-grid uk-text-right">
							<li>
								<div class="element uk-float-left" style="background: url('images/home_volunteer_2.jpg')"></div>
							</li>
							<li>
								<div class="element uk-float-right" style="background: url('images/home_volunteer_1.jpg')"></div>
							</li>
							<li>
								<div class="element uk-float-left" style="background: url('images/home_volunteer_4.jpg')"></div>
							</li>
							<li>
								<div class="element uk-float-right" style="background: url('images/home_volunteer_3.jpg')"></div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="blog uk-vertical-align" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
			<div class="uk-container uk-container-center ">
				<div class="uk-grid uk-vertical-align-middle">
					<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2">
						<img src="images/home_blog.jpg">
					</div>
					<div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2 uk-text-center ">
						<div class="content ">
							<h1>Educatie</h1>
							<span class="block">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</span>
							<a href="#!" class="uk-button dark-green-bg">mai mult</a>
						</div>
					</div>
				</div>
				<hr class="divider">
			</div>
		</section>
		<section class="partners" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
			<div class="uk-container uk-container-center">
                <div class="uk-grid">
                    <div class="uk-width-1-1">
						<ul class="inline-block uk-text-center">
							<li><a href="#!"><img src="images/Artmark_logo.jpg"></a></li>
							<li><a href="#!"><img src="images/logo_polaris.png"></a></li>
						</ul>
                    </div>
                </div>
            </div>
		</section>
	</div>


	<?php include("templates/footer.php") ?>
