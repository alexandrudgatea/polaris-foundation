<section id="testimonials" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-1-1 uk-text-center">
				<h1>Testimoniale</h1>
				<div class="uk-margin" data-uk-slideset="{small: 1, medium: 2, large: 3}">
					<div class="uk-slidenav-position uk-margin">
						<ul class="uk-slideset uk-grid uk-flex-center uk-grid-width-1-3">
							<li>
								<div class="testimonial">
									<div class="testimonial-image">
										<img src="images/test3.jpg" class="uk-border-circle">
									</div>
									<span class="block testimonial-text">Privesc viața din toate unghiurile, pentru că viaţa se lasă privită.</span>
									<span class="block testimonial-author"> Matei Gabriel, Medic</span>
								</div>
							</li>
							<li>
								<div class="testimonial">
									<div class="testimonial-image">
										<img src="images/test2.jpg" class="uk-border-circle">
									</div>
									<span class="block testimonial-text">Privesc viața din toate unghiurile, pentru că viaţa se lasă privită.</span>
									<span class="block testimonial-author"> Matei Gabriela, Stomatolog</span>
								</div>
							</li>
							<li>
								<div class="testimonial">
									<div class="testimonial-image">
										<img src="images/test1.jpg" class="uk-border-circle">
									</div>
									<span class="block testimonial-text">Privesc viața din toate unghiurile, pentru că viaţa se lasă privită.</span>
									<span class="block testimonial-author"> Ionescu Mihaela, Fotograf</span>
								</div>
							</li>
							<li>
								<div class="testimonial">
									<div class="testimonial-image">
										<img src="images/test3.jpg" class="uk-border-circle">
									</div>
									<span class="block testimonial-text">Privesc viața din toate unghiurile, pentru că viaţa se lasă privită.</span>
									<span class="block testimonial-author"> Matei Gabriel, Medic</span>
								</div>
							</li>
							<li>
								<div class="testimonial">
									<div class="testimonial-image">
										<img src="images/test3.jpg" class="uk-border-circle">
									</div>
									<span class="block testimonial-text">Privesc viața din toate unghiurile, pentru că viaţa se lasă privită.</span>
									<span class="block testimonial-author"> Matei Gabriel, Medic</span>
								</div>
							</li>
							
						</ul>
					</div>
					<ul class="uk-slideset-nav uk-dotnav uk-flex-center"></ul>
				</div>
			</div>
		</div>
	</div>
</section>
