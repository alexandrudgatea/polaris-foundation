<section id="footer">
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-small-1-1 uk-width-medium-1-2">
				<form class="uk-form" id="newsletter">
					<fieldset>
						<legend>Abonaza-te la newsletter</legend>
						<div class="uk-form-row newsletter-text uk-width-4-6">
							Esti interesat de activitatea noastra? Aboneaza-te la newsletter si te vom tine la curent cu toate noutatile!
						</div>
						<div class="uk-form-row">
							<input type="text" placeholder="Adresa de email" id="nws-email" name="nws-email">
							<button type="submit" class="uk-button">Ma abonez</button>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="uk-width-small-1-1 uk-width-medium-1-2">
				<div class="uk-float-right contact-column">
					<h1>Contacteaza-ne</h1>
					<ul class="uk-text-right">
						<li><a href="mailto:contact@fundatiapolaris.ro">contact@fundatiapolaris.ro <i class="uk-icon-envelope"></i></a></li>
						<li><a href="tel:0745495979">0745495979 <i class="uk-icon-phone"></i></a></li>
					</ul>
				</div>
			</div>
		</div>

	</div>
	<div class="uk-grid">
		<div class="uk-width-1-1">
			<ul class="footer-bottom">
				<li>© Copyrighted 2016-2017 </li>
				<li>Fundatia Polaris</li>
				<li>Toate drepturile rezervate.</li>
			</ul>
		</div>
	</div>
</section>

</body>

</html>
