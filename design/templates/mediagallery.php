<section id="mediaGallery" style="background: url('images/green_bg.jpg')" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-1-1">
				<h1>Galerie media</h1>
				<ul id="box-container" class="media-gallery inline-block">
					<li class="box">
						<a rel="gallery" href="images/mg1.jpg" class="swipebox" title="Title of image">
							<img src="images/mg1.jpg" alt="image">
						</a>
					</li>
					<li class="box">
						<a rel="gallery" class="swipebox-video" href="https://www.youtube.com/watch?v=hSE_rWW8Kk0">
							<img src="http://img.youtube.com/vi/hSE_rWW8Kk0/0.jpg">
						</a>
					</li>
					<li class="box">
						<a rel="gallery" class="swipebox-video" href="https://www.youtube.com/watch?v=hSE_rWW8Kk0">
							<img src="http://img.youtube.com/vi/hSE_rWW8Kk0/0.jpg">
						</a>
					</li>
					<li class="box">
						<a href="images/mg3.jpg" class="swipebox" title="Title of image">
							<img src="images/mg3.jpg" alt="image">
						</a>
					</li>
					<li class="box">
						<a href="images/mg4.jpg" class="swipebox" title="Title of image">
							<img src="images/mg4.jpg" alt="image">
						</a>
					</li>
					<li class="box">
						<a href="images/mg5.jpg" class="swipebox" title="Title of image">
							<img src="images/mg5.jpg" alt="image">
						</a>
					</li>
					<li class="box">
						<a href="images/mg5.jpg" class="swipebox" title="Title of image">
							<img src="images/mg5.jpg" alt="image">
						</a>
					</li>
					<li class="box">
						<a href="images/mg7.jpg" class="swipebox" title="Title of image">
							<img src="images/mg7.jpg" alt="image">
						</a>
					</li>
					<li class="box">
						<a href="images/mg8.jpg" class="swipebox" title="Title of image">
							<img src="images/mg8.jpg" alt="image">
						</a>
					</li>
					<li class="box">
						<a href="images/mg2.jpg" class="swipebox" title="Title of image">
							<img src="images/mg2.jpg" alt="image">
						</a>
					</li>
					<li class="box">
						<a href="images/mg3.jpg" class="swipebox" title="Title of image">
							<img src="images/mg3.jpg" alt="image">
						</a>
					</li>
					<li class="box">
						<a href="images/mg4.jpg" class="swipebox" title="Title of image">
							<img src="images/mg4.jpg" alt="image">
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	jQuery(document).ready(function($) {
		// initialize image lightbox
		$('.swipebox').swipebox();

		// initialize video lightbox
		$('.swipebox-video').swipebox();

		$('.jcarousel.video a').click(function(e) {
			e.preventDefault();
		});

		// check if the image is has landscape or portrait orientation and stile acordingly
		$(window).load(function() {
			$('#mediaGallery').find('img').each(function() {
				var imgClass = (this.width / this.height > 1) ? 'wide' : 'tall';
				$(this).addClass(imgClass);
			});
		});
	});

</script>
