<!DOCTYPE html>
<html lang="">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Fundatia Polaris</title>
	<link rel="shortcut icon" href="">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/uikit.min.css">
	<link rel="stylesheet" href="css/styles.css">
	<link rel="stylesheet" href="css/swipebox.min.css">
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/jquery.swipebox.min.js"></script>
	<script src="js/uikit.min.js"></script>
	<script src="js/parallax.min.js"></script>
	<script src="js/slideset.min.js"></script>
	<!--[if IE]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<nav class="uk-navbar" id="menu">
		<div class="uk-container uk-container-center">
			<div class="uk-grid">
				<div class="uk-width-1-1">

					<!-- logo visible on large devices-->
					<a href="home.php" class="uk-navbar-brand uk-hidden-small">
						<img src="images/fundatie_logo.jpg">
					</a>

					<!-- navbar visible on large devices-->
					<ul class="uk-navbar-nav uk-navbar-flip uk-hidden-small desktop-menu">
						<li class="uk-active"><a href="home.php"><i class="uk-icon-home"></i></a></li>
						<li><a href="event.php">Evenimente</a></li>
						<li><a href="blog.php">Educatie</a></li>
						<li><a href="volunteer.php">Voluntariat</a></li>
						<li class="donatii"><a href="donation.php">Donatii</a></li>
						<li><a href="contact.php">Contact</a></li>
					</ul>

					<!-- the toggle of off-canvas side menu-->
					<a href="#mobileSidemenu" data-uk-offcanvas>
						<span class="uk-navbar-toggle uk-visible-small"></span>
					</a>

					<!-- logo visible on small and medium devices-->
					<a href="home.php" class="uk-navbar-brand uk-navbar-center uk-visible-small">
						<img src="images/fundatie_logo.jpg">
					</a>
				</div>
			</div>
		</div>
	</nav>

	<!-- This is the off-canvas sidebar -->
	<div id="mobileSidemenu" class="uk-offcanvas">
		<div class="uk-offcanvas-bar">
			<ul class="uk-nav">
				<li class="uk-active"><a href="home.php"><i class="uk-icon-home"></i></a></li>
				<li><a href="event.php">Evenimente</a></li>
				<li><a href="blog.php">Educatie</a></li>
				<li><a href="volunteer.php">Voluntariat</a></li>
				<li class="donatii"><a href="donation.php">Donatii</a></li>
				<li><a href="contact.php">Contact</a></li>
			</ul>
		</div>
	</div>
