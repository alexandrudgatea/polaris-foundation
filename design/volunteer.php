<?php include("templates/header.php") ?>

<div id="volunteer" class="page">
    <section id="hero" style="background-image: url('images/volunteer_bg.jpg')" data-uk-parallax="{bg: '150'}">
    </section>

    <div class="uk-container uk-container-center">
        <div class="uk-grid">
            <div class="uk-width-1-2" data-uk-scrollspy="{cls:'uk-animation-slide-left'}">
                <div class="vol-wrap">
                    <h1>Programul de voluntariat</h1>
                    <span class="block intro">
					    Iti doresti o implicare activa ca voluntar la Fundatia Polaris? Iti palce sa ajuti, sa fii util? Doresti sa participi la actiunile noastre de sprijinire a ONG-urilor si la crearea de evenimente caritabile? Mai jos gasesti informatii despre cum ni te poti alatura.
				    </span>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                        mollit anim id est laborum.</p>
                    <h2>Formular de inscriere</h2>
                    <div class="signup-fields"></div>
                    <a href="#!" class="uk-button dark-green-bg">Vreau sa fiu voluntar</a>
                </div>
            </div>
            <div class="uk-width-1-2" data-uk-scrollspy="{cls:'uk-animation-slide-right'}">
                <div class="vol-images">
                    <div class="uk-grid">
                        <div class="uk-width-2-3">
                            <div class="img img1" style="background-image: url('images/v_img1.jpg')"></div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="uk-grid">
                                <div class="uk-width-1-1">
                                    <div class="img img2" style="background-image: url('images/v_img2.jpg')"></div>
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-1-1">
                                    <div class="img img2 dark-green-bg"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div class="img img3" style="background-image: url('images/v_img3.jpg')"></div>
                        </div>
                    </div>
                    <div class="uk-grid">
                        <div class="uk-width-1-3">
                            <div class="img img3 green-bg"></div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="img img3" style="background-image: url('images/v_img4.jpg')"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include("templates/footer.php") ?>

