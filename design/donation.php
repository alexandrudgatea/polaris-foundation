<?php include("templates/header.php") ?>

	<div id="donation" class="page">
		<section id="hero" style="background-image: url('images/donation_bg.jpg')" data-uk-parallax="{bg: '150'}">

		</section>
		<section class="donation-card" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">
					<div class="uk-width-2-3 uk-container-center">
						<div class="card">
							<h1>Contribuie si tu!</h1>
							<span class="block donation-intro">
								Directioneaza 2% din impozitul pe venit Fundatiti Polaris. Foloseste unul din cele 2 formulare de mai jos:
							</span>
							<ul class="inline-block donation-method">
								<li class="method1 uk-text-center uk-vertical-align">
									<div class="method-content uk-vertical-align-middle">
										<h1>Declaratia 230</h1>
										<span class="block">pentru venituri obtinute  din salariul in anul fiscal precedent</span>
										<a href="#!" class="uk-button bordered">Descarca</a>
									</div>

								</li>
								<li class="method2 uk-text-center uk-vertical-align">
									<div class="method-content uk-vertical-align-middle">
										<h1>Declaratia 200</h1>
										<span class="block">pentru venituri obtinute  din activitati independente in alul fiscal precedent</span>
										<a href="#!" class="uk-button bordered">Descarca</a>
									</div>
								</li>
							</ul>
							<span class="block send-option">
								Directioneaza 2% din impozitul pe venit Fundatiti Polaris. Foloseste unul din cele 2 formulare de mai jos:
								<ol>
									<li>E-mail, scanat, la adresa <a href="mailto:fundatiapolaris@polarismedical.ro">fundatiapolaris@polarismedical.ro</a> </li>
									<li>Il depui personal la Administratia Financiara de care apartii.  </li>
								</ol>
							</span>
							<hr class="divider">
							<span class="block more-about">
								Daca doresti sa aflii mai multe despre organizatiile si proiectele pe care le sustinem, contacteaza-ne!
							</span>
							<div class="card-footer">
								<h1>Multumim!</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<?php include("templates/footer.php") ?>
