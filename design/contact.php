<?php include("templates/header.php") ?>

    <div id="contact" class="page">
        <section id="hero" style="background-image: url('images/contact_bg.jpg')" data-uk-parallax="{bg: '150'}">
        </section>
        <div id="googlemaps" style="pointer-events:none;"></div>
        <div id="contactform">
            <h1>Contacteaza-ne!</h1>
            <p>Pentru orice intrebare ne poti contacta telefonic sau prin e-mail
                sau poti folosi formularul de mai jos.</p>
            
            <a href="tel:0264323323"><i class="uk-icon-phone" aria-hidden="true"></i> 0264 - 323 323</a>
            <br>
            
            <a href="mailto:fundatiapolaris@polarismedical.ro"><i class="uk-icon-envelope" aria-hidden="true"></i> fundatiapolaris@polarismedical.ro</a>
            <h2>Formular de contact</h2>
            <div class="contact-fields"></div>
            <a href="#!" class="uk-button dark-green-bg">Trimite</a>

        </div>
    </div>

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript">

        // The latitude and longitude of your business / place
        var position = [46.794529, 23.482915];

        function showGoogleMaps() {

            var latLng = new google.maps.LatLng(position[0], position[1]-0.01);
            var latLngMarker = new google.maps.LatLng(position[0], position[1]);

            var mapOptions = {
                zoom: 15, // initialize zoom level - the max value is 21
                streetViewControl: false, // hide the yellow Street View pegman
                scaleControl: false, // allow users to zoom the Google Map
                draggable: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: latLng
            };

            map = new google.maps.Map(document.getElementById('googlemaps'),
                mapOptions);

            // Show the default red marker at the location
            marker = new google.maps.Marker({
                position: latLngMarker,
                map: map,
                draggable: false,
                scaleControl: false,
                animation: google.maps.Animation.DROP
            });
        }

        google.maps.event.addDomListener(window, 'load', showGoogleMaps);
    </script>

<?php include("templates/footer.php") ?>